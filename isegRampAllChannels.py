'''
(c) Copyright CERN 2019. All  rights  reserved.  This  software  is  released  under  a 
CERN proprietary  software  licence.  Any  permission to  use  it  shall  be  granted
in  writing.  Requests shall be addressed to CERN through mail 
- author(s)
- KT@cern.ch

author(s): 
    Michael Ludwig BE-ICS-FD michael.ludwig@cern.ch
'''
# CAEN OPCUA tests (venus)
# simple read/write and increment test on scalars, and arrays
# Python 2.7.5 on cc7 3.10.0-957.21.2.el7.x86_64
#
# export PYTHONPATH=`pwd`:~/3rdPartySoftware/uaf/lib

#import numpy as np
import ctypes
import time
import sys

import pyuaf
from pyuaf.util             import Address, NodeId
from pyuaf.client           import Client
from pyuaf.client.settings  import ClientSettings
from pyuaf.util.primitives  import Byte, UInt32, Int32, Float, Double, String

# internal macros
NOT_FOUND = "not found"

# ---------------------


# read a numerical property from a controller. Crate is implicitly global,
# myclient is global as well
def readControllerProp(prop): 
    addr = Address(NodeId(globalCan0+"."+prop, globalName), globalUrn )  
    print("reading %s" %addr.getExpandedNodeId())
    result = myClient.read( [addr] )
    print("addr overallStatus= %s" %result.overallStatus )
    print("addr targets.data= %s" %result.targets[0].data )
    print("result.targets[0].data= ", result.targets[0].data )
    if isinstance(result.targets[0].data, Float):   # <pyuaf.util.primitives.Float(0)>
        return Float(str(result.targets[0].data))
    elif isinstance(result.targets[0].data, Int32): # <pyuaf.util.primitives.Int32(0)>
        return Int32(str(result.targets[0].data))
    elif isinstance(result.targets[0].data, UInt32): # <pyuaf.util.primitives.Int32(0)>
        return UInt32(str(result.targets[0].data))
    elif isinstance(result.targets[0].data, String): # <pyuaf.util.primitives.String(A1676)>
        return str(result.targets[0].data)
    else: 
        return NOT_FOUND


# read a property from a given module (iseg: either a crate or a module with channels), 
# controller is implicit global, myclient is global as well
# node names are just concatenated with a seperating dot (.)
# returns: 
#    float if OK
#    "not found" if it does not exists
def readModuleProp(module, prop): 
    addr = Address(NodeId(globalCan0+"."+module+"."+prop, globalName), globalUrn )  
    print("reading %s" %addr.getExpandedNodeId())
    result = myClient.read( [addr] )
    #print("result.targets[0].data= ", result.targets[0].data )    
    if isinstance(result.targets[0].data, Float):   # <pyuaf.util.primitives.Float(0)>
        return Float(str(result.targets[0].data))
    elif isinstance(result.targets[0].data, Int32): # <pyuaf.util.primitives.Int32(0)>
        return Int32(str(result.targets[0].data))
    elif isinstance(result.targets[0].data, UInt32): # <pyuaf.util.primitives.Int32(0)>
        return UInt32(str(result.targets[0].data))
    elif isinstance(result.targets[0].data, String):  # <pyuaf.util.primitives.String(A1676)>
        return str(result.targets[0].data)
    else: 
        return NOT_FOUND


# print out namespace, uri etc of the server discovery from pyuaf
def checkServer(): 
    addr = Address(NodeId(globalCan0+".ModuleList", globalName), globalUrn )  
    print("reading %s" %addr.getExpandedNodeId())
    result = myClient.read( [addr] )
    print("addr overallStatus= %s" %result.overallStatus )
    print("addr targets.data= %s" %result.targets[0].data )
    print("result.targets[0].data= ", result.targets[0].data )
    print("result= ", result )
    print("===made it up to here, caen seems good===")

# set a number for a given channel, crate is implicit global, myclient is global as well
# use JCOP naming conventions: Board00 Chan000
# crate and board are integers
# we are actually writing floats, and rely on internal conversion of types
def writeChPropNumerical(board, channel, prop, newvalue): 
    sboard = ".Board"+str( board ).zfill(2)
    schannel = ".Chan"+str( channel ).zfill(3)   
    addr = Address(NodeId(globalCrate0+sboard+schannel+"."+prop, globalName), globalUrn )  
    result = myClient.write( [addr], [ Float(newvalue) ] )

# tries to read Model and returns a list of populated primary slots in the crate (=boards)
# obviously only populated slots can return a board "Model", so the other ones 
# are empty
def tryBoards( maxSlots ):
    populatedSlots = []
    emptySlots = []
    for slot in range (maxSlots):
        result = readBdProp(slot, "Model")
        #print( "result= ", result, isinstance(result, str))
        if isinstance(result, str):
            if ( result == NOT_FOUND ):
                #print("slot ", slot, " is empty")
                emptySlots.append(slot)
            else:
                #print("slot ", slot, " has a board ", result)
                populatedSlots.append(slot)
        else:
            populatedSlots.append(slot)
    return populatedSlots
    
# create a client(connection) named "myClient". We need:
# -port: configured in ServerConfig.xml
# -ip or hostname: computer
# - name of the crate=globalCrate0: as configured to the adress space (config.xml)
# -uri=globalUrn: as configured in the ServerConfig.xml
# -global name=namespace in the adress space: configured in ServerConfig.xml
serverHostNameDefault = "localhost"
opcUaPortDefault = "4901"
globalCan0 = "can0"
globalUrn = "urn:JCOP:OpcUaIsegServer"
globalName = "opcuaserver" 
# the urn and uri are configured in the according ServerConfig.xml, at startup of the OPCUACaenServer
'''
  <ApplicationUri>urn:JCOP:OpcUaIsegServer</ApplicationUri>
  <ServerUri>urn:[NodeName]:JCOP:OpcUaIsegServer</ServerUri>
'''

    
myClient = Client(ClientSettings("pyuaf_ramp_all_channels", ["opc.tcp://localhost:4899"]))
print( "checking server")
checkServer()


#print( "hit any key to start")
#sys.stdin.readline()


# find out, by trying, how many primary boards are actually present, and return a 
# vector with populated slots = boards
moduleList = readControllerProp("ModuleList")
print("moduleList= ", moduleList )

cycleCounter = readControllerProp("CycleCounter")
print("cycleCounter= ", cycleCounter )

moduleTemperature = readModuleProp("module000000", "Temperature")
print("moduleTemperature= ", moduleTemperature )


